<?php
include_once("IModel.php");
include_once("Book.php");

/** The Model is the class holding data about a collection of books.
 * @author Rune Hjelsvold
 * @see http://php-html.net/tutorials/model-view-controller-in-php/ The tutorial code used as basis.
 */
class DBModel implements IModel
{
    /**
     * The PDO object for interfacing the database
     *
     */
    protected $db = null;

    /**
     * @throws PDOException
     */
    public function __construct($db = null)
    {
        if ($db)
        {
            $this->db = $db;
        }
        else
        {
            $this->db = $db = new PDO('mysql:host=' . DB_HOST . ';dbname=' . DB_NAME . ';charset=utf8mb4', DB_USER, DB_PWD);
        }

        $this->db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    }

    /** Function returning the complete list of books in the collection. Books are
     * returned in order of id.
     * @return Book[] An array of book objects indexed and ordered by their id.
     * @throws PDOException
     */
    public function getBookList()
    {
        $stmt = $this->db->query("SELECT * FROM Book ORDER BY id");

        return array_map(function($row) {
            return new Book($row['title'], $row['author'], $row['description'], $row['id']);
        }, $stmt->fetchAll(PDO::FETCH_ASSOC));
    }

    /** Function retrieving information about a given book in the collection.
     * @param integer $id the id of the book to be retrieved
     * @return Book|null The book matching the $id exists in the collection; null otherwise.
     * @throws PDOException
     */
    public function getBookById($id)
    {
        $stmt = $this->db->prepare("SELECT * FROM Book WHERE id=:id");

        $stmt->bindValue("id", $id, PDO::PARAM_INT);

        $stmt->execute();
        $row = $stmt->fetch(PDO::FETCH_ASSOC);

        if($row) {
            return new Book($row['title'], $row['author'], $row['description'], $id);
        } else {
            return null;
        }
    }

    /** Adds a new book to the collection.
     * @param $book Book The book to be added - the id of the book will be set after successful insertion.
     * @throws PDOException
     */
    public function addBook($book)
    {
        $stmt = $this->db->prepare(
            "INSERT into Book (title, author, description) VALUES (:title, :author, :description)"
        );

        $stmt->bindValue("title", $book->title);
        $stmt->bindValue("author", $book->author);
        $stmt->bindValue("description", $book->description);

        $stmt->execute();

        $book->id = $this->db->lastInsertId();
    }

    /** Modifies data related to a book in the collection.
     * @param $book Book The book data to be kept.
     * @todo Implement function using PDO and a real database.
     */
    public function modifyBook($book)
    {
        $stmt = $this->db->prepare(
            "UPDATE Book SET title = :title, author = :author, description = :description WHERE id = :id"
        );

        $stmt->bindValue("id", $book->id);
        $stmt->bindValue("title", $book->title);
        $stmt->bindValue("author", $book->author);
        $stmt->bindValue("description", $book->description);

        $stmt->execute();
    }

    /** Deletes data related to a book from the collection.
     * @param $id integer The id of the book that should be removed from the collection.
     */
    public function deleteBook($id)
    {
        $stmt = $this->db->prepare("DELETE FROM Book WHERE id = :id");

        $stmt->bindValue("id", $id, PDO::PARAM_INT);

        $stmt->execute();
    }
}

?>