# README #

To set up the system do the following:

* Download and install docker
* Inside the root of the repository run `docker-compose up`
* Enjoy the web app on localhost:8080
