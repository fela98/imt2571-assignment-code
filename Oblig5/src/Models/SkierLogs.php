<?php

namespace Models;

class SkierLogs
{
    /**
     * @var Skier[] $entries
     */
    public $skiers;

    /**
     * @var Club[] $entries
     */
    public $clubs;

    /**
     * @var SkierSeason[] $skierSeasons
     */
    public $skierSeasons;

    /**
     * @var Entry[] $entries
     */
    public $entries;
}