<?php

namespace Models;

use DateTime;

class Entry
{
    /**
     * @var DateTime date
     */
    public $date;

    /**
     * @var string $area
     */
    public $area;

    /**
     * @var int $distance
     */
    public $distance;

    /**
     * @var string $userName
     */
    public $userName;

    /**
     * @var int $season
     */
    public $season;
}