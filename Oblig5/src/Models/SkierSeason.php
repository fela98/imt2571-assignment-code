<?php

namespace Models;

class SkierSeason
{
    /**
     * @var string $userName
     */
    public $userName;

    /**
     * @var string $clubId
     */
    public $clubId;

    /**
     * @var int $season
     */
    public $season;

    /**
     * @var int $totalDistance
     */
    public $totalDistance;
}