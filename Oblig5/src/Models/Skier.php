<?php

namespace Models;

class Skier
{
    /**
     * @var $userName string
     */
    public $userName;

    /**
     * @var $firstName string
     */
    public $firstName;

    /**
     * @var $lastName string
     */
    public $lastName;

    /**
     * @var $yearOfBirth int
     */
    public $yearOfBirth;
}
