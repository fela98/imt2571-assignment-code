<?php

namespace Models;

class Club
{
    /**
     * @var string $id
     */
    public $id;

    /**
     * @var string $name
     */
    public $name;

    /**
     * @var string $city
     */
    public $city;

    /**
     * @var string $county
     */
    public $county;
}