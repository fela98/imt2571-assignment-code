<?php

include_once "Models/Entry.php";
include_once "Models/Club.php";
include_once "Models/Skier.php";
include_once "Models/SkierLogs.php";
include_once "Models/SkierSeason.php";
include_once "DbModel.php";
include_once "SkierLogsXmlParser.php";

$xmlDoc = new DOMDocument();
$xmlDoc ->load("../SkierLogs.xml");

$x = $xmlDoc->documentElement;

$parser = new SkierLogsXmlParser();

$skierLogs = $parser->parseSkierLogs($x);

$dbModel = new DbModel();

foreach($skierLogs->skiers as $skier) {
    $dbModel->saveSkier($skier);
}

foreach($skierLogs->clubs as $club) {
    $dbModel->saveClub($club);
}

foreach($skierLogs->skierSeasons as $skierSeason) {
    $dbModel->saveSkierSeason($skierSeason);
}

foreach($skierLogs->entries as $entry) {
    $dbModel->saveEntry($entry);
}

$dbModel->updateTotalSeasonDistances();

echo "Finished!";