<?php

use Models\Club;
use Models\Entry;
use Models\Skier;
use Models\SkierSeason;

class DbModel
{
    /**
     * @var PDO
     */
    protected $db;

    public function __construct($db = null)
    {
        if ($db)
        {
            $this->db = $db;
        }
        else
        {
            $this->db = $db = new PDO('mysql:host=felix-mysqldb.mysql.database.azure.com;dbname=skiers;charset=utf8mb4', 'user@felix-mysqldb', 'aiSh8uHi');
        }

        $this->db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    }

    public function saveSkier(Skier $skier) {
        $stmt = $this->db->prepare(
            "INSERT INTO skier (userName, firstName, lastName, yearOfBirth) VALUES (:userName, :firstName, :lastName, :yearOfBirth)"
        );

        $stmt->bindValue("userName", $skier->userName);
        $stmt->bindValue("firstName", $skier->firstName);
        $stmt->bindValue("lastName", $skier->lastName);
        $stmt->bindValue("yearOfBirth", $skier->yearOfBirth, PDO::PARAM_INT);

        $stmt->execute();
    }

    public function saveClub(Club $club) {
        $stmt = $this->db->prepare(
            "INSERT into club (clubId, clubName, city, county) VALUES(:clubId, :clubName,:city, :county)"
        );

        $stmt->bindValue("clubId", $club->id);
        $stmt->bindValue("clubName", $club->name);
        $stmt->bindValue("city", $club->city);
        $stmt->bindValue("county", $club->county);

        $stmt->execute();
    }

    public function saveSkierSeason(SkierSeason $skierSeason) {
        $stmt = $this->db->prepare(
            "INSERT INTO skierseason (userName, season, clubId, totalDistance) VALUES (:userName, :season, :clubId, :totalDistance)"
        );

        $stmt->bindValue("userName", $skierSeason->userName);
        $stmt->bindValue("season", $skierSeason->season, PDO::PARAM_INT);
        $stmt->bindValue("clubId", $skierSeason->clubId);
        $stmt->bindValue("totalDistance", $skierSeason->totalDistance, PDO::PARAM_INT);

        $stmt->execute();
    }

    public function saveEntry(Entry $entry) {

        $stmt = $this->db->prepare(
            "INSERT INTO entry (userName, date, season, area, distance) VALUES (:userName, :date, :season, :area, :distance)"
        );

        $stmt->bindValue("userName", $entry->userName);
        $stmt->bindValue("date", $entry->date);
        $stmt->bindValue("season", $entry->season, PDO::PARAM_INT);
        $stmt->bindValue("area", $entry->area);
        $stmt->bindValue("distance", $entry->distance, PDO::PARAM_INT);

        $stmt->execute();
    }

    public function updateTotalSeasonDistances() {
        $this->db->query(
            "UPDATE skierseason
            SET totalDistance = (
              SELECT SUM(distance)
              FROM entry
              WHERE userName = skierseason.userName AND season = skierseason.season
            )"
        );
    }
}