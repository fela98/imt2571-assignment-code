<?php

use Models\Club;
use Models\Entry;
use Models\Skier;
use Models\SkierLogs;
use Models\SkierSeason;

class SkierLogsXmlParser
{
    /**
     * @var Club[] clubs
     */
    private $clubs;

    /**
     * @var Skier[] $skiers
     */
    private $skiers;

    /**
     * @var Entry[] $entries
     */
    private $entries;

    public function parseSkierLogs(DOMElement $skierLogsElement): SkierLogs {
        /**
         * @var DOMElement $clubsElement
         */
        $clubsElement = $this->getChild($skierLogsElement, "Clubs");

        $clubs = array_map(function($clubElement) {
            return $this->parseClub($clubElement);
        }, iterator_to_array($clubsElement->getElementsByTagName("Club")));

        $skiersElement = $this->getChild($skierLogsElement, "Skiers");

        $skiers = array_map(function($skierElement) {
            return $this->parseSkier($skierElement);
        }, iterator_to_array($skiersElement->getElementsByTagName("Skier")));

        $skierLogs = new SkierLogs();

        $skierLogs->clubs = $clubs;
        $skierLogs->skiers = $skiers;

        $skierSeasons = array();

        $seasonElements = $skierLogsElement->getElementsByTagName("Season");

        foreach ($seasonElements as $seasonElement) {
             $skierSeasons = array_merge($this->parseSeason($seasonElement), $skierSeasons);
        }

        $skierLogs->skierSeasons = $skierSeasons;
        $skierLogs->entries = $this->entries;

        return $skierLogs;
    }

    private function parseClub(DOMElement $clubElement): Club {
        $club = new Club();

        $club->id = $clubElement->getAttribute("id");
        $club->name = $this->getChildValue($clubElement, "Name");
        $club->city = $this->getChildValue($clubElement, "City");
        $club->county = $this->getChildValue($clubElement, "County");

        return $club;
    }

    private function parseSkier(DOMElement $skierElement): Skier {
        $skier = new Skier();

        $skier->userName = $skierElement->getAttribute("userName");
        $skier->firstName = $this->getChildValue($skierElement, "FirstName");
        $skier->lastName = $this->getChildValue($skierElement, "LastName");
        $skier->yearOfBirth = (int)$this->getChildValue($skierElement, "YearOfBirth");

        return $skier;
    }

    /***
     * @param DOMElement $seasonElement
     * @return SkierSeason[]
     */
    private function parseSeason(DOMElement $seasonElement) {
        $skierSeasons = array();

        $fallYear = (int)$seasonElement->getAttribute("fallYear");
        /** @var DOMElement $skiersElement */
        foreach($seasonElement->getElementsByTagName("Skiers") as $skiersElement) {
            $clubId = $skiersElement->getAttribute("clubId");

            /** @var DOMElement $skierElement */
            foreach ($skiersElement->getElementsByTagName("Skier") as $skierElement) {
                $skierSeason = new SkierSeason();
                $userName = $skierElement->getAttribute("userName");

                $skierSeason->userName = $userName;
                $skierSeason->clubId = $clubId;
                $skierSeason->season = $fallYear;
                $skierSeason->totalDistance = 0;

                foreach($this->parseEntries($skierElement, $skierSeason) as $entry) {
                    $this->entries[] = $entry;
                }

                $skierSeasons[] = $skierSeason;
            }
        }

        return $skierSeasons;
    }

    private function parseEntries(DOMElement $skierSeasonElement, SkierSeason $skierSeason) {
        $entries = array();

        $logElement = $this->getChild($skierSeasonElement,"Log");
        $entryElements = $logElement->getElementsByTagName("Entry");

        foreach($entryElements as $entryElement) {
            $entries[] = $this->parseEntry($entryElement, $skierSeason);
        }

        return $entries;
    }

    private function parseEntry(DOMElement $entryElement, SkierSeason $skierSeason): Entry {
        $entry = new Entry();

        $entry->date = $this->getChildValue($entryElement, "Date");
        $entry->area = $this->getChildValue($entryElement, "Area");
        $entry->distance = (int)$this->getChildValue($entryElement, "Distance");

        $entry->season = $skierSeason->season;
        $entry->userName = $skierSeason->userName;

        return $entry;
    }

    private function getChildValue(DOMElement $parent, string $childName) {
        return $this->getChild($parent, $childName)->nodeValue;
    }

    private function getChild(DOMElement $parent, string $childName) {
        return $parent->getElementsByTagName($childName)->item(0);
    }
}